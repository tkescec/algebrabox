<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserCategories extends Model
{
    protected $table = 'user_categories';


	public function categories(){
		return $this->belongsTo('App\Models\Categories', 'category_id');
	}
}
