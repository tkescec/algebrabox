<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Users extends Model
{
    protected $table = 'users';

    /**
     * Get the categories record associated with the user.
     */
	public function categories()
	{
		return $this->belongsToMany('App\Models\Categories', 'user_categories','user_id','category_id');
	}
}
