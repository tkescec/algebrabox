<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UsersRootMap extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'users_root_map';
}
