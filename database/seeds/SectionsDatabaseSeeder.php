<?php

use Illuminate\Database\Seeder;

class SectionsDatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('sections')->insert(array(
            array('name' => 'images'),
            array('name' => 'documents'),
            array('name' => 'musics'),
            array('name' => 'videos'),
        ));
    }
}
