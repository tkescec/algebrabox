<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class MigrationUserRootMaps extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users_root_map', function (Blueprint $table) {
            $table->increments('id');
			$table->string('name');
			$table->integer('users_id')->unsigned();
            $table->timestamps();

			#Index
			$table->index('users_id');

			#Foreign key
			$table->foreign('users_id')->references('id')->on('users')->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users_root_map');
    }
}
