<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Categories extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'categories';

    /**
     * Get the section record associated with the category.
     */
    public function sections()
    {
        return $this->belongsTo('App\Models\Sections');
    }

    /**
     * Get the users that owns the category.
     */
    public function users()
    {
        return $this->belongsToMany('App\Models\Users', 'user_categories','category_id','user_id');
    }
}
